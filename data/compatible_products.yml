# Sample output to create a set with a compatible product
# sets:
#  - title: Sparkplug 1.0 Platform Software
#    release: ""
#    type: "software"
#    items:
#    - name: Eclipse Glassfish
#      vendor: Eclipse Foundation
#      image: '/images/compatible-products/glassfish.png'
#      image_width: 125
#      download: 'https://www.eclipse.org/downloads/download.php?file=/glassfish/glassfish-5.1.0.zip'
#      versions:
#      - version: 5.1.0
#        compatibility: 'https://eclipse-ee4j.github.io/glassfish/certifications/jakarta-full-profile/8.0/TCK-Results'
#        download_url: 'https://www.eclipse.org/downloads/download.php?file=/glassfish/glassfish-5.1.0.zip'
#
# In the case of Sparkplug, the first `download` field is what the product tile gets linked to. 
# It doesn't necessarily need to be a download url unlike what the field name implies.

sets:
  - sparkplug_version: 3.0
    items:
      - title: Sparkplug 3.0 Platform Software
        release: ""
        type: software
        items: 
          - name: Canary MQTT Collector
            vendor: Canary Labs 
            image: /images/compatible-products/canary-labs-logo.svg
            download: https://helpcenter.canarylabs.com/t/h7hvlzk/mqtt-collector-overview-version-23
            image_width: 150
            versions:
              - version: v23.0.0
                download_url: https://helpcenter.canarylabs.com/t/h7hvlzk/mqtt-collector-overview-version-23
                compatibility: https://helpcenter.canarylabs.com/t/q6yqg6q/sparkplugb-3-0-compliance-version-23
          - name: Canary Publisher 
            vendor: Canary Labs 
            image: /images/compatible-products/canary-labs-logo.svg
            download: https://helpcenter.canarylabs.com/t/m1hvlty/publisher-overview-version-23 
            image_width: 150
            versions:
              - version: v23.0.0
                download_url: https://helpcenter.canarylabs.com/t/m1hvlty/publisher-overview-version-23 
                compatibility: https://helpcenter.canarylabs.com/t/60yqgw8/publisher-sparkplugb-30-compliance-version-23 
          - name: Cirrus Link Chariot MQTT Server
            vendor: Cirrus Link Solutions
            image: /images/compatible-products/chariot-mqtt-server-logo.png
            download: https://cirrus-link.com/mqtt-broker-iiot-mqtt-servers/
            versions:
              - version: v2.3.0
                download_url: https://chariot-releases.s3.amazonaws.com/2.3.0/chariot_linux.zip
                compatbility: https://docs.chariot.io/display/CHAR2x/Chariot+v2.3.0+Compliance
          - name: Ignition by Inductive Automation
            vendor: Inductive Automation
            image: /images/compatible-products/ignition-logo.png
            download: https://inductiveautomation.com/ignition
            versions:
              - version: v8.1.23
                download_url: /https://inductiveautomation.com/downloads/
                compatibility: https://github.com/eclipse-sparkplug/sparkplug.listings/issues/8
          - name: N3uron Client for Sparkplug
            vendor: N3uron Connectivity Systems
            image: /images/compatible-products/n3uron-logo.svg
            download: https://n3uron.com/iiot-platform-modules/sparkplug-client/
            versions:
              - version: v1.2.0
                download_url: https://n3uron.com/iiot-platform-modules/sparkplug-client/
                compatibility: https://n3uron.com/wp-content/uploads/2023/01/n3clientforsparkplugv1.2.0compliance.html
          - name: Cirrus Link MQTT Transmission
            vendor: Cirrus Link Solutions
            image: "/images/compatible-products/mqtt-transmission-logo.png"
            image_width: 95
            download: https://cirrus-link.com/mqtt-software-for-iiot-scada/
            versions:
              - version: v4.0.14
                download_url: https://inductiveautomation.com/downloads/third-party-modules/8.1.23
                compatibility: https://docs.chariot.io/display/CLD80/MQTT+Transmission+v4.0.14+Compliance
          - name: Cirrus Link MQTT Engine
            vendor: Cirrus Link Solutions
            image: "/images/compatible-products/mqtt-engine-logo.png"
            image_width: 95
            download: https://cirrus-link.com/mqtt-software-for-iiot-scada/
            versions:
              - version: v4.0.14
                download_url: https://inductiveautomation.com/downloads/third-party-modules/8.1.23
                compatibility: https://docs.chariot.io/display/CLD80/MQTT+Engine+v4.0.14+Compliance
          - name: Cirrus Link MQTT Distributor
            vendor: Cirrus Link Solutions
            image: "/images/compatible-products/mqtt-distributor-logo.png"
            image_width: 95
            download: https://cirrus-link.com/mqtt-software-for-iiot-scada/
            versions:
              - version: v4.0.14
                download_url: https://inductiveautomation.com/downloads/third-party-modules/8.1.23
                compatibility: https://docs.chariot.io/display/CLD80/MQTT+Distributor+v4.0.14+Compliance
          - name: HiveMQ MQTT Broker Professional Edition
            image: "/images/compatible-products/hivemq-logo.png"
            image_width: 100
            download: 'https://www.hivemq.com/hivemq/mqtt-broker/'
            versions:
              - version: v4.9.1
                download_url: https://www.hivemq.com/releases/hivemq-4.9.1.zip
          - name: HiveMQ MQTT Broker Community Edition
            image: '/images/compatible-products/hivemq-logo.png'
            image_width: 100
            download: 'https://github.com/hivemq/hivemq-community-edition/'
            versions:
              - version: v2022.1
                compatibility: https://www.hivemq.com/verification/summary.html
                download_url: 'https://github.com/hivemq/hivemq-community-edition/releases/download/2022.1/hivemq-ce-2022.1.zip'
          - name: Eclipse Tahu
            vendor: Eclipse IoT
            image: "/images/compatible-products/eclipse-tahu-logo.png"
            image_width: 125
            download: "https://github.com/eclipse/tahu"
            versions:
              - version: Tahu Core v1.0.0
                download_url: https://www.eclipse.org/tahu/downloads/java/v1.0.0/tahu-core-1.0.0.jar
              - version: Tahu Edge v1.0.0
                download_url: http://www.eclipse.org/tahu/downloads/java/v1.0.0/tahu-edge-1.0.0.jar
              - version: Tahu Host v1.0.0
                download_url: http://www.eclipse.org/tahu/downloads/java/v1.0.0/tahu-host-1.0.0.jar
              - version: Tahu Edge Compatibility v1.0.0
                download_url: http://www.eclipse.org/tahu/downloads/java/v1.0.0/tahu-edge-compat-1.0.0.jar
              - version: Tahu Host Compatibility v1.0.0
                download_url: http://www.eclipse.org/tahu/downloads/java/v1.0.0/tahu-host-compat-1.0.0.jar
          - name: HiveMQ Sparkplug Aware Extension
            vendor: Eclipse IoT
            image: "/images/compatible-products/hivemq-logo.png"
            image_width: 100
            download: "https://github.com/hivemq/hivemq-sparkplug-aware-extension"
          - name: Litmus Sparkplug Edge Node
            vendor: Litmus
            image: "/images/compatible-products/litmus-sparkplug-edge-node.png"
            image_width: 150
            download: https://central.litmus.io/listing/le-sparkplug-edge-node/
            versions:
              - version: v1.3.1
                compatibility: https://central.litmus.io/sparkplug-tck-results-summary/
                download_url: https://central.litmus.io/products/le-sparkplug-edge-node/
          - name: NanoMQ MQTT Server
            vendor: NanoMQ
            image: "/images/compatible-products/nanomq-mqtt-server-logo.webp"
            image_width: 150
            download: https://nanomq.io/downloads
            versions:
              - version: v0.17.9
                download_url: https://github.com/emqx/nanomq/releases/tag/0.17.9
          - name: SIOTH Master Broker
            vendor: Integration Objects
            image: "/images/compatible-products/integration-objects-logo.webp"
            image_width: 150
            download: https://integrationobjects.com/sioth/
            versions:
              - version: v2.3.1
                download_url: https://integrationobjects.com/sioth/
          - name: SIOTH MQTT Universal Broker
            vendor: Integration Objects
            image: "/images/compatible-products/integration-objects-logo.webp"
            image_width: 150
            download: https://integrationobjects.com/sioth-opc/industrial-mqtt/mqtt-universal-broker/
            versions:
              - version: v2.3.1
                download_url: https://integrationobjects.com/sioth-opc/industrial-mqtt/mqtt-universal-broker/
          - name: Smart IOT Highway
            vendor: Integration Objects
            image: "/images/compatible-products/integration-objects-logo.webp"
            image_width: 150
            download: https://integrationobjects.com/sioth/
            versions:
              - version: v2.3.1
                download_url: https://integrationobjects.com/sioth/
      - title: Sparkplug 3.0 Platform Hardware
        release: ""
        type: "hardware"
        items:
          - name: Opto 22 groov EPIC GRV-EPIC-PR1
            vendor: Opto 22
            image: /images/compatible-products/groov-epic.png
            download: https://www.opto22.com/products/product-container/grv-epic-pr1
            versions:
              - version: Firmware 3.5.0-b.56
                download_url: https://www.opto22.com/products/product-container/grv-epic-pr1
                compatibility: https://downloads.opto22.com/EPIC-3.5.0-TCK-Results.html
              - version: Firmware 3.6.0-b.32
                download_url: https://www.opto22.com/products/product-container/grv-epic-pr1
                compatibility: https://downloads.opto22.com/EPIC-3.6.0-TCK-Results.html
          - name: Opto 22 groov EPIC GRV-EPIC-PR2
            vendor: Opto 22
            image: /images/compatible-products/groov-epic.png
            download: https://www.opto22.com/products/product-container/grv-epic-pr2
            versions:
              - version: Firmware 3.5.0-b.56
                download_url: https://www.opto22.com/products/product-container/grv-epic-pr2
                compatibility: https://downloads.opto22.com/EPIC-3.5.0-TCK-Results.html
              - version: Firmware 3.6.0-b.32
                download_url: https://www.opto22.com/products/product-container/grv-epic-pr2
                compatibility: https://downloads.opto22.com/EPIC-3.6.0-TCK-Results.html
          - name: Opto 22 groov RIO GRV-R7-I1VAPM-3
            vendor: Opto 22
            image: /images/compatible-products/groov-rio.png
            download: https://www.opto22.com/products/product-container/grv-r7-i1vapm-3
            versions:
              - version: Firmware 3.5.0-b.46
                download_url: https://www.opto22.com/products/product-container/grv-r7-i1vapm-3
                compatibility: https://downloads.opto22.com/RIO-3.5.0-TCK-Results.html
          - name: Opto 22 groov RIO GRV-R7-MM1001-10
            vendor: Opto 22
            image: /images/compatible-products/groov-rio.png
            download: https://www.opto22.com/products/product-container/grv-r7-mm1001-10
            versions:
              - version: Firmware 3.5.0-b.46
                download_url: https://www.opto22.com/products/product-container/grv-r7-mm1001-10
                compatibility: https://downloads.opto22.com/RIO-3.5.0-TCK-Results.html
          - name: Opto 22 groov RIO GRV-R7-MM2001-10
            vendor: Opto 22
            image: /images/compatible-products/groov-rio.png
            download: https://www.opto22.com/products/product-container/grv-r7-mm2001-10
            versions:
              - version: Firmware 3.5.0-b.46
                download_url: https://www.opto22.com/products/product-container/grv-r7-mm2001-10
                compatibility: https://downloads.opto22.com/RIO-3.5.0-TCK-Results.html
          - name: SignalFire Ranger
            vendor: SignalFire Telemetry
            image: /images/compatible-products/ranger-logo.png
            download: https://www.signal-fire.com/lte-m1-cellular-products/ranger-node/
            versions:
              - version: v0.1.27
                download_url: https://www.signal-fire.com/lte-m1-cellular-products/ranger-node/
                compatibility: https://www.signal-fire.com/summary.html
