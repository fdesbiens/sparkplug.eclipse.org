---
title: "The Sparkplug specification"
date: 2022-10-19T10:19:45-04:00
description: | 
    Sparkplug is an open-source specification hosted at the Eclipse Foundation that provides MQTT clients the framework to seamlessly integrate data from their applications, sensors, devices, and gateways within the MQTT Infrastructure.
keywords: ['eclipse sparkplug', 'specification', 'compatibility', 'sparkplug versions', 'revision', 'committee']
toc: false
draft: false
hide_sidebar: true
layout: single
---

Sparkplug is an open-source specification hosted at the Eclipse Foundation that provides MQTT clients the framework to seamlessly integrate data from their applications, sensors, devices, and gateways within the MQTT Infrastructure. [It is developed in the open, on GitHub](https://github.com/eclipse/sparkplug). The evolution of Sparkplug is governed by the [Eclipse Foundation Specification Process (EFSP)](https://www.eclipse.org/projects/efsp/). Anyone can copy and distribute the specification documents in any medium for any purpose and without fee or royalty.

The aim of the Sparkplug Specification is to define an MQTT Topic Namespace, payload, and session state management that can be applied generically to the overall IIoT market sector but specifically meets the requirements of real-time SCADA/Control HMI solutions. Meeting the operational requirements for these systems will enable MQTT-based infrastructures to provide more valuable real-time information to Line of Business and MES solution requirements as well.

## Revision History

{{< specification/revision_history >}}

## Specification Committee

The Specification Committee is responsible for implementing the ​Eclipse Foundation Specification Process (EFSP) ​for all Specification Projects under the purview of the Sparkplug Working Group. This committee ensures that EFSP is followed and votes to approve creation reviews, progress reviews, and release reviews submitted by the [Sparkplug specification project](https://projects.eclipse.org/projects/iot.sparkplug).

### Resources
- [Sparkplug Compatible Software](/compatibility/compatible-software/)
- [Sparkplug Compatible Hardware](/compatibility/compatible-hardware/)
- [Sparkplug TCK process](/specification/tck-process/)

### Committee Members

As defined in the [Charter of the Sparkplug working group](https://www.eclipse.org/org/workinggroups/eclipse_sparkplug_charter.php), strategic-level members each have a seat on the specification committee. Participant-level members elect a representative among their ranks.

The current members of the committee are:

{{< specification/committee type="specification" >}}

## Meeting Minutes

No meeting minutes have been published at this time.