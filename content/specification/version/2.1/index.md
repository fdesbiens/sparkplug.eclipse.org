---
title: "Sparkplug version 2.1"
author: Cirrus Link Solutions
version_number: "2.1"
version_description: |
    Payload B Addition
version_date: 2016-12-10
specification_pdf: null
type: specification_version
description: ""
categories: []
keywords: []
slug: ""
aliases: []
redirect_url: "../../"
---
