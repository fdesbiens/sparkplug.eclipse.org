---
title: "Sparkplug version 3.0"
author: Sparkplug Specification Project Team
version_number: "3.0"
version_description: |
    Migrated to AsciiDoc. 
    Completely reorganized.  
    Added explicit normative and non-normative statements.
version_date: 2022-10-21
specification_pdf: documents/sparkplug-specification-3.0.0.pdf
type: specification_version
description: The specification of Sparkplug version 3.0
keywords: ['sparkplug version', 'sparkplug specification', 'ballots', 'vote', 'compatible', 'compatibility']
---

Sparkplug 3.0 represents the first version of the specification managed under the Eclipse Foundation specification process. It represents a proper formalization of the specification over the v2.2 release. The goals were to clarify ambiguities in the v2.2 version and add explicit normative statements where needed without adding new features that would break existing devices and applications.

## Details

- [Sparkplug v3.0 Release Record](https://projects.eclipse.org/projects/iot.sparkplug/releases/3.0.0)
- [Sparkplug v3.0 Specification Document (PDF)](/specification/version/3.0/documents/sparkplug-specification-3.0.0.pdf)
- [Sparkplug v3.0 TCK](https://download.eclipse.org/sparkplug/3.0.0/Eclipse-Sparkplug-TCK-3.0.0.zip) ([sig](https://download.eclipse.org/sparkplug/3.0.0/Eclipse-Sparkplug-TCK-3.0.0.zip.sig))

## Compatible Implementations used for ratification

- [Eclipse Tahu v1.0.0](https://github.com/eclipse/tahu/releases/tag/v1.0.0)
- [HiveMQ Sparkplug Aware Extension v.4.8.2](https://github.com/hivemq/hivemq-sparkplug-aware-extension/releases/tag/4.8.2)

## Compatible Implementations

- [Sparkplug Compatible Software](https://sparkplug.eclipse.org/compatibility/compatible-software/)
- [Sparkplug Compatible Hardware](https://sparkplug.eclipse.org/compatibility/compatible-hardware/)

## Ballots

Ratification

The Specification Committee Ballot concluded successfully on October 21, 2022 with the following results.

{{< specification/ballots version="3.0" >}}

<small>*Votes were not received in time from HiveMQ and the participant member representative.</small>
