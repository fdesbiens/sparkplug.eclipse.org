---
title: "Sparkplug version 1.0"
author: Cirrus Link Solutions
version_number: "1.0"
version_description: |
    Initial Release
version_date: 2016-05-26
specification_pdf: null 
type: specification_version
description: ""
categories: []
keywords: []
slug: ""
aliases: []
redirect_url: "../../"
---
