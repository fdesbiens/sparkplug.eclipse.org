---
title: Committee Minutes
seo_title: Committee Minutes | Sparkplug Working Group
hide_sidebar: true
---

You will find the meeting minutes for the Sparkplug working group's steering committee on this page. You can also expect to find the meeting minutes from the specification committee in the near future.

## Steering Committee

{{< eclipsefdn_meeting_minutes yearly_sections_enabled="true" >}}