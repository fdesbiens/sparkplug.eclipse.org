+++
date = "2022-03-10"
title = "Ask Me Anything Webinar: March Edition | FAQs on MQTT, Sparkplug, & HiveMQ Answered by the Experts"
link = "https://www.youtube.com/watch?v=f3xYU_-DeRk"
link_class  = "eclipsefdn-video"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT", "HiveMQ"]
categories = ["video"]
+++

In the March 2022 edition of HiveMQ webinar, 'Ask Me Anything' About MQTT,  Florian Raschbichler, Head of Support at HiveMQ, and Jens Deters, Head of Professional Services at HiveMQ, along with Gaurav Suman, Director - Product Marketing at HiveMQ, answered some of the most pressing questions around MQTT 5, Sparkplug specification, MQTT Security, ISA 95, OPC-UA, AMPQ, IIoT use cases, etc.
