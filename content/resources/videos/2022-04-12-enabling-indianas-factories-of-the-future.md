+++
date = "2022-04-12"
title = "Enabling Indiana’s Factories of the Future"
link = "https://cirrus-link.com/videos/enabling-indianas-factories-of-the-future/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://i.vimeocdn.com/video/1412832608-370c879e92c8b8c21147c5c5c5288fce5f333390cb228ca993b53726c54e1014-d?mw=720"
tags = [ "video", "open source", "Eclipse", "sparkplug", "Indiana", "Factories"]
categories = ["video"]
+++

Learn about how Indiana is leading the way in advancing manufacturers to make Industry 4.0 quick, simple, and cost-effective. Indiana has launched a first of a kind program to enable their manufacturing industries to become the Factories of the Future. The program engages with manufacturing companies across Indiana to integrate the I4.0 Accelerator solution powered by AWS that seamlessly connects data from factory equipment to enable energy management systems, using AI/data analytics, to optimize energy utilization and provide ML insights. The result is projected to be a reduction in energy costs of between 8-20% depending on the factory’s current efficiency of operations.


Agenda:
* Indiana’s Energy Insights Initiative
* AWS Energy Insights MVP
* Machine to Cloud connectivity technology components & demo
* Energy Insights AI / ML powered insights Demo