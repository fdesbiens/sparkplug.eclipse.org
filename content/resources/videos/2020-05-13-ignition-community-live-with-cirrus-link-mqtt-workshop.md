+++
date = "2020-05-13"
title = "Ignition Community Live with Cirrus Link: MQTT Workshop"
link = "https://cirrus-link.com/videos/ignition-community-live-with-cirrus-link-mqtt-workshop/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://embed-fastly.wistia.com/deliveries/a8d3f0f97be437ea6be348a6bca9895c.jpg?image_crop_resized=720x405"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT", "IIoT"]
categories = ["video"]
+++

A workshop building a complete end-to-end IIoT connected solution from scratch. Learn how to easily connect data to from the Edge to Ignition and beyond. See partners around the world connect in live showing the real power of MQTT.
