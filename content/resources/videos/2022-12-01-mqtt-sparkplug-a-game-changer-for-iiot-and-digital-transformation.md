+++
date = "2022-12-01"
title = "MQTT Sparkplug: A Game Changer for IIoT and Digital Transformation"
link = "https://www.youtube.com/watch?v=AfG9OaWyk20"
link_class = "eclipsefdn-video"
tags = ["video", "eclipse", "sparkplug", "mqtt", "iiot", "digital transformation"]
categories = ["video"]
+++

Watch this webinar to learn:

- How Sparkplug can bring data interoperability and connect your manufacturing machines and processes with enterprise data centers
- The unique features of Sparkplug that provide additional benefits to manufacturing customers
- Details about the new Sparkplug 3.0 specification released by the Eclipse Sparkplug Working Group, and what that means to current/future customers and vendors
- How HiveMQ can help guide customers in their Industry 4.0 journey with Sparkplug and Unified Name Space (UNS)
- How Sparkplug compares to other similar standards like OPC UA and ISA 95 when it comes to data modeling
- Common customer use cases for Sparkplug