+++
date = "2021-08-02"
title = "MQTT and the Purdue Model: IIoT Security Best Practices"
link = "https://cirrus-link.com/mqtt-and-the-purdue-model-iiot-security-best-practices/"
+++

The Purdue Model of Computer Integrated Manufacturing was published in 1990 as a reference for enterprise architecture – a hierarchical model for manufacturing automation that defines what type of equipment goes where what actions are supposed to happen at each level, and how machines and processes interact.
