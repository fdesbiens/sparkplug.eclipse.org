+++
date = "2022-02-02"
title = "Application software: Updates, connections and design considerations"
link = "https://www.automationworld.com/process/workforce/article/22006009/solving-the-industrial-skills-gap"
+++

Your process control applications should just work and communicate with your devices—from one update to the next
